﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControllWork6.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControllWork6.Controller
{
    [Produces("application/json")]
    [Route("api/Cafe")]
    public class CafeController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public CafeController(ApplicationDbContext context)
        {
            this.context = context;
        }

        // GET: api/Cafe
        [HttpGet]
        public string GetAllCafe()
        {
            var cafe = context.Cafe.ToList();
            return JsonConvert.SerializeObject(cafe, Formatting.Indented);
        }

        // GET: api/Cafe/5
        [HttpGet("{id}", Name = "GetCafe")]
        public string GetCafe(int id)
        {
            var cafe = context.Cafe.ToList().Where(x => x.Id == id).FirstOrDefault();
            return cafe.Name;
        }

        // POST: api/Cafe
        [HttpPost]
        public string PostCafe([FromForm] Cafe cafe)
        {
            context.Cafe.Add(cafe);
            context.SaveChanges();

            return GetAllCafe();
        }

        // PUT: api/Cafe/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
