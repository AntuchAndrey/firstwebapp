﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControllWork6.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControllWork6.Controller
{
    [Produces("application/json")]
    [Route("api/Dish")]
    public class DishController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public DishController(ApplicationDbContext context)
        {
            this.context = context;
        }
        // GET: api/Dish
        [HttpGet]
        public string GetAllDish()
        {
            var dish = context.Dishes.ToList();
            return JsonConvert.SerializeObject(dish, Formatting.Indented);
        }

        // GET: api/Dish/5
        [HttpGet("{id}", Name = "GetDish")]
        public string GetDish(int id)
        {
            var dish = context.Dishes.ToList().Where(x => x.Id == id).FirstOrDefault();
            return dish.Name;
        }
        
        [HttpPost]
        public string PostCafe([FromForm] Dish dish)
        {
            context.Dishes.Add(dish);
            context.SaveChanges();

            return GetAllDish();
        }
        
        // PUT: api/Dish/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
