﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControllWork6.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControllWork6.Controller
{
    [Produces("application/json")]
    [Route("api/Client")]
    public class ClientController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public ClientController(ApplicationDbContext context)
        {
            this.context = context;
        }
        // GET: api/Client
        [HttpGet]
        public string GetAllClients()
        {
            var client = context.Clients.ToList();
            return JsonConvert.SerializeObject(client, Formatting.Indented);
        }

        // GET: api/Client/5
        [HttpGet("{id}", Name = "GetClient")]
        public string GetClient(int id)
        {
            var clients = context.Clients.ToList().Where(x => x.Id == id).FirstOrDefault();
            return clients.Name;
        }

        // POST: api/Client
        [HttpPost]
        public string PostCafe([FromForm] Client client)
        {
            context.Clients.Add(client);
            context.SaveChanges();

            return GetAllClients();
        }


        // PUT: api/Client/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
