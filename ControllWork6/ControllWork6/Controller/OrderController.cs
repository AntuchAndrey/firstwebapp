﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControllWork6.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControllWork6.Controller
{
    [Produces("application/json")]
    [Route("api/Order")]
    public class OrderController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public OrderController(ApplicationDbContext context)
        {
            this.context = context;
        }
        // GET: api/Dish
        [HttpGet]
        public string GetAllOrders()
        {
            var order = context.Orders.ToList();
            return JsonConvert.SerializeObject(order, Formatting.Indented);
        }

        // GET: api/Dish/5
        [HttpGet("{id}", Name = "GetOrder")]
        public Order GetOrder(int id)
        {
            var order = context.Orders.ToList().Where(x => x.Id == id).FirstOrDefault();
            return order;
        }

        [HttpPost]
        public string PostCafe([FromForm] Order order)
        {
            order.DataOrder = DateTime.Now;
            context.Orders.Add(order);
            context.SaveChanges();
            return GetAllOrders();
        }

        // PUT: api/Order/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
