﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControllWork6.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Cafe> Cafe { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Order> Orders { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Order>()
                .HasOne(o => o.Dish)
                .WithMany(o => o.Orders)
                .HasForeignKey(o => o.DishId);

            modelBuilder.Entity<Dish>()
                .HasMany(d => d.Orders)
                .WithOne(d => d.Dish)
                .HasPrincipalKey(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Order>()
               .HasOne(o => o.Client)
               .WithMany(o => o.Orders)
               .HasForeignKey(o => o.ClientId);

            modelBuilder.Entity<Client>()
                .HasMany(c => c.Orders)
                .WithOne(c => c.Client)
                .HasPrincipalKey(c => c.Id);

            modelBuilder.Entity<Cafe>()
                .HasMany(cf => cf.Dish)
                .WithOne(cf => cf.Cafe)
                .HasPrincipalKey(cf => cf.Id);

            modelBuilder.Entity<Dish>()
                .HasOne(d => d.Cafe)
                .WithMany(d => d.Dish)
                .HasForeignKey(d => d.CafeId);



            //modelBuilder.Entity<Order>()
            //    .HasOne(o => o.Phone)
            //    .WithMany(o => o.Orders)
            //    .HasForeignKey(o => o.PhoneId);

            //modelBuilder.Entity<Phone>()
            //    .HasMany(p => p.Orders)
            //    .WithOne(p => p.Phone)
            //    .HasPrincipalKey(p => p.Id)
            //    .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<Phone>()
            //    .HasOne(p => p.Category)
            //    .WithMany(p => p.Phones)
            //    .HasForeignKey(p => p.CategoryId);

            //modelBuilder.Entity<Category>()
            //    .HasMany(c => c.Phones)
            //    .WithOne(c => c.Category)
            //    .HasPrincipalKey(c => c.Id)
            //    .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<Category>()
            //    .HasMany(c => c.SubCategories)
            //    .WithOne(c => c.ParentCategory)
            //    .HasForeignKey(c => c.ParentCategoryId);

            //modelBuilder.Entity<Category>()
            //    .HasOne(c => c.ParentCategory)
            //    .WithMany(c => c.SubCategories)
            //    .HasPrincipalKey(c => c.Id)
            //    .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<PhoneOnStock>()
            //    .HasKey(ps => new { ps.PhoneId, ps.StockId });

            //modelBuilder.Entity<PhoneOnStock>()
            //    .HasOne(p => p.Stock)
            //    .WithMany(p => p.Phones)
            //    .HasForeignKey(p => p.StockId);

            //modelBuilder.Entity<PhoneOnStock>()
            //    .HasOne(p => p.Stock)
            //    .WithMany(p => p.Phones)
            //    .HasForeignKey(p => p.StockId);
        }
    }
}
