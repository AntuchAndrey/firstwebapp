﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControllWork6.Models
{
    public class Client
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string Contact { get; set; }

        public IEnumerable<Order> Orders { get; set; }
    }
}
