﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControllWork6.Models
{
    public class Order
    {
        public int Id { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }

        public int DishId { get; set; }
        public Dish Dish { get; set; }

        public DateTime DataOrder { get; set; }
    }
}
